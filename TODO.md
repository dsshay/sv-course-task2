Применяя TDD, разработать мультивалютный счёт с возможностью снятия/внесения средств, перевода между счетами.

- [x] Write TODO
- [x] Write test to account
- [x] Write test to bank

Base functionality in Bank class:
- [x] Add client operations
- [x] Add account operations
- [x] List of currency and currency rates
- [x] add transfer money
- [x] Operations of withdraw and add to account

Base functionality in Account class:
- [x] Show balance
- [x] Change balance
- [x] Converter

- [x] Static analyzer
- [x] Dynamic analyzer
- [ ] Add test coverage
import random
from bank_account_interaction.account import Account
from bank_account_interaction.account import Rates

Currencies = list(Rates.keys())


def init_account():
    for el in range(100000000):
        currency = random.choice(Currencies)
        accounts = [Account(currency, random.uniform(0, 100), random.uniform(0, 100))]
        return accounts


def change_balance(accounts):
    for i in accounts:
        for j in range(1000000):
            currency = random.choice(Currencies)
            i.change_balance(random.uniform(1000, 3000), tp='+', currency=currency)
    for i in accounts:
        for j in range(1000000):
            currency = random.choice(Currencies)
            i.change_balance(random.uniform(0, 10), tp='-', currency=currency)


acc = init_account()
change_balance(acc)

import bank_account_interaction.account as account


class Bank:
    def __init__(self):
        self.rates = account.Rates
        self.currencies = self.rates.keys()
        self.clients = {}
        self.clients_identifications = {}
        self.accounts = {}

    def add_client(self, name):
        """ Add person to list of client.

        :param name: name of client
        :return: client_id in banking_system
        """
        if len(name) != 0:
            ids = len(self.clients.keys())
            self.clients_identifications[ids] = {name}
            self.clients[ids] = []
            return ids
        else:
            raise ValueError('Clients name %s is error' % name)

    def add_account(self, client_id, name, currency, balance=0):
        """ Create a new client account.

        :params:
            client_id: client id in banking system;
            currency: currency of account;
            balance: account balance at opening. Default is 0;
        :return: account_id in banking system
        """
        if client_id in self.clients_identifications.keys():
            if len(name) != 0:
                if currency in self.currencies:
                    account_id = len(self.accounts.keys())
                    acc_obj = account.Account(currency, account_id, balance)
                    self.accounts[account_id] = acc_obj
                    self.clients[client_id].append(account_id)
                    return account_id
                else:
                    raise ValueError(
                        'Choose account currency from %s' % self.currencies)
        else:
            raise ValueError(
                "There is no such client ID %s in the bank's system. "
                "Check the client %s in the bank's system " % (
                    client_id, name))

    def transfer_money(self, amount, currency, source, dest):
        """ Transfer operations.

        :params:
            amount: amount of operation
            currency: currency of operation
            source: account_id of source transfer
            destination: account_id of target transfer
        """
        if source in self.accounts.keys() and dest in self.accounts.keys():
            if currency in self.currencies:
                if amount > 0:
                    self.accounts[source].change_balance(amount, tp='-',
                                                         currency=currency)
                    self.accounts[dest].change_balance(amount, tp='+',
                                                       currency=currency)
                elif amount < 0:
                    raise ValueError('Incorrect amount of operation')
            else:
                raise ValueError('Incorrect the currency of operation')
        else:
            raise ValueError('Incorrect the debit and/or receipt account')

    def withdraw_add(self, account_id, amount, currency='rub', tp='-'):
        """Withdraw from account and add to acount

        :params account_id: account on which the operation is performed
        amount: accordingly currency: accordingly tp: type of operation.
        Default '-', which means withdraw from account, type '+' means add
        money to account
        """
        if account_id in self.accounts.keys():
            if currency in self.currencies:
                if tp == '+':
                    self.accounts[account_id].change_balance(amount, tp='+',
                                                             currency=currency)
                elif tp == '-':
                    self.accounts[account_id].change_balance(amount, tp='-',
                                                             currency=currency)
                else:
                    raise ValueError('ATM operation error')
            else:
                raise ValueError('Incorrect the currency of operation')
        else:
            raise ValueError('Incorrect account_id')

Rates = {'rub': 1.0, 'usd': 73.68, 'eur': 90.10}


class Account:
    def __init__(self, currency, account_id, start_balance):
        """ Initialize new account. """
        if not (isinstance(start_balance, int)
                or isinstance(start_balance, float)) or start_balance < 0:
            raise ValueError('Not correct balance')
        if currency not in Rates.keys():
            raise ValueError('Unknown currency')
        self.currency = currency
        self.balance = start_balance
        self.id = account_id

    def change_balance(self, amount, tp='-', currency=None):
        """ Change balance

        :params: amount: amount od opearion tp: type of operation.
        Depositing (+) or withdrawing (-) money from an account currency:
        currency of operation
        """
        additional_term = amount * convert(currency, self.currency)
        if tp == '+':
            self.balance += additional_term
        elif tp == '-':
            if self.balance >= additional_term:
                self.balance -= additional_term
            else:
                raise ValueError('Not enough money')
        else:
            raise ValueError('Unknown operation type')

    def show_balance(self):
        return self.balance


def convert(currency_old, currency_new):
    """Converter
        :params:
            amount: amount of money
            currency_old: source currency
            currency_new: target currency
        :return: Converted value in new currency
        """
    if currency_old not in Rates.keys() or currency_new not in Rates.keys():
        raise ValueError('Unknown transaction currency')
    converted_value = round(Rates[currency_old] / Rates[currency_new], 5)
    return converted_value

import unittest
import pytest
import bank_account_interaction.account as account
import bank_account_interaction.bank as bank


class TestAccount(unittest.TestCase):
    def setUp(self):
        self.account = account.Account('rub', account_id=1, start_balance=0)

    def test_ini(self):
        """
        Test Account initialization
        """
        assert self.account.currency == 'rub'
        assert self.account.balance == 0
        assert self.account.id == 1
        with pytest.raises(ValueError):
            account.Account('rub', account_id=1, start_balance='')
        with pytest.raises(ValueError):
            account.Account('rub', account_id=1, start_balance=-100)
        with pytest.raises(ValueError):
            account.Account('not existing currency', account_id=1,
                            start_balance=-100)
        with pytest.raises(ValueError):
            account.Account('not existing currency', account_id=1,
                            start_balance=100)

    def test_change_balance(self):
        """
        Test functional of changing account balance
        """
        assert self.account.change_balance(1, tp='+', currency='rub') is None
        assert self.account.balance == 1
        assert self.account.change_balance(1, tp='-', currency='rub') is None
        assert self.account.balance == 0
        assert self.account.change_balance(1, tp='+', currency='usd') is None
        assert self.account.balance == 73.68
        with pytest.raises(ValueError):
            self.account.change_balance(1, tp='', currency='usd')

    def test_show_balance(self):
        assert self.account.show_balance() == 0


class TestBank(unittest.TestCase):
    def setUp(self):
        self.bank = bank.Bank()

    def test_ini(self):
        """
        Test Bank initialization.

        1) Check that at least one currency value is present in the currency
        exchange rate
        2) Check that at least one currency is in turnover
        """
        # assert self.bank.rates is not None
        assert len(self.bank.rates.keys()) > 0
        # assert self.bank.currencies is not None
        assert len(self.bank.currencies) > 0

    def test_add_client(self):
        """
        Test functional adding users to the bank's system

        1) Check not empty input data
        2) Check not empty ids of client
        3) Check that client id is integer
        4) Check that if add client with similar name, ids will be different
        """
        self.bank = bank.Bank()
        # 1
        with pytest.raises(ValueError):
            self.bank.add_client('')
        # 2
        result_first = self.bank.add_client('Danil')
        assert result_first is not None
        # 3
        assert isinstance(result_first, int)
        # 4
        result_second = self.bank.add_client('Danil')
        assert result_first != result_second

    def test_add_account(self):
        """
        Test functional adding account to exist client in bank's system
        1) Check not empty input data
        2) - 7) Check incorrect input data
        8) Check returned account id is integer
        9) Check that if add account with similar param, ids will be different
        """
        # 1
        self.bank = bank.Bank()
        with pytest.raises(ValueError):
            self.bank.add_account(client_id='', name='',
                                  currency='', balance='')
        # 2
        with pytest.raises(ValueError):
            self.bank.add_account(client_id='bla', name='bla',
                                  currency='bla', balance='bla')
        with pytest.raises(ValueError):
            ids = self.bank.add_client('Danil')
            self.bank.add_account(client_id='bla', name='Danil',
                                  currency='bla', balance='bla')
        with pytest.raises(ValueError):
            self.bank.add_account(client_id=ids, name='Danil',
                                  currency='bla', balance='bla')
        with pytest.raises(ValueError):
            self.bank.add_account(client_id=ids, name='Danil',
                                  currency='rus', balance='bla')
        with pytest.raises(ValueError):
            self.bank.add_account(client_id=ids, name='Danil',
                                  currency='rus', balance=-1)
        with pytest.raises(ValueError):
            self.bank.add_account(client_id=ids, name='Danil',
                                  currency='rus')
        # 8
        ids = self.bank.add_client('Danil')
        result_first = self.bank.add_account(client_id=ids, name='Danil',
                                             currency='rub')
        assert isinstance(result_first, int)
        # 9
        result_second = self.bank.add_account(client_id=ids, name='Danil',
                                              currency='rub')
        assert result_first != result_second
        # 10
        ids = self.bank.add_client('Maria')
        assert self.bank.add_account(client_id=ids, name='',
                                     currency='rub') is None

    def test_transfer_money(self):
        """
        Test functional transferring money
        1) Check transfer to yourself 0 money
        2) Check transfer to not existing account
        3) - 4) Check incorrect input data
        5) Check incorrect transfer 0 money from and to existing accounts
        6) Check insufficient funds for transfer
        7) Check sign of money
        8) Check incorrect transfer
        9) Check correct change of the sender's account balance
        10)Check correct change of the recipient's account balance
        """
        # 1
        self.bank = bank.Bank()
        id_danil = self.bank.add_client('Danil')
        account_id_danil = self.bank.add_account(client_id=id_danil,
                                                 name='Danil', currency='rub')
        assert self.bank.transfer_money(amount=0, currency='rub',
                                        source=account_id_danil,
                                        dest=account_id_danil) is None
        # 2
        with pytest.raises(ValueError):
            self.bank.transfer_money(amount=0, currency='rub',
                                     source=account_id_danil, dest=1234)
        # 3
        with pytest.raises(ValueError):
            self.bank.transfer_money(amount=0, currency='rub',
                                     source=account_id_danil, dest='')
        # 4
        with pytest.raises(ValueError):
            self.bank.transfer_money(amount=0, currency='rub',
                                     source='', dest=account_id_danil)
        # 5
        id_friend = self.bank.add_client('Friend')
        account_id_friend = self.bank.add_account(client_id=id_friend,
                                                  name='Friend',
                                                  currency='rub')
        assert self.bank.transfer_money(amount=0, currency='rub',
                                        source=account_id_danil,
                                        dest=account_id_friend) is None
        # 6
        with pytest.raises(ValueError):
            self.bank.transfer_money(amount=100, currency='rub',
                                     source=account_id_danil,
                                     dest=account_id_friend)
        # 7
        with pytest.raises(ValueError):
            self.bank.transfer_money(amount=-100, currency='rub',
                                     source=account_id_danil,
                                     dest=account_id_friend)
        # 8
        id_somebody = self.bank.add_client('Somebody')
        account_id_somebody = self.bank.add_account(client_id=id_somebody,
                                                    name='Somebody',
                                                    currency='rub',
                                                    balance=1000)
        assert self.bank.transfer_money(amount=100, currency='rub',
                                        source=account_id_somebody,
                                        dest=account_id_danil) is None
        # 9
        assert self.bank.accounts[account_id_somebody].balance == 900
        # 10
        assert self.bank.accounts[account_id_danil].balance == 100
        # 11
        with pytest.raises(ValueError):
            self.bank.transfer_money(amount=1, currency='bla',
                                     source=account_id_danil,
                                     dest=account_id_friend)

    def test_withdraw_add(self):
        """
        Test functional withdraw money from acount and add to account

        1) Check incorrect input
        2) Check withdraw operations
        3) Check add operations
        4) Check add in usd
        """
        # 1
        self.bank = bank.Bank()
        client_id = self.bank.add_client('Danil')
        account_id = self.bank.add_account(client_id=client_id, name='Danil',
                                           currency='rub', balance=100)
        with pytest.raises(ValueError):
            self.bank.withdraw_add('', '', '', '')
        # 2
        assert self.bank.withdraw_add(account_id, 50) is None
        assert self.bank.accounts[account_id].balance == 50

        # 3
        assert self.bank.withdraw_add(account_id, 500, tp='+') is None
        assert self.bank.accounts[account_id].balance == 550

        # 4
        assert self.bank.withdraw_add(account_id, 500,
                                      currency='usd', tp='+') is None
        assert self.bank.accounts[account_id].balance == 37390.0

        # 5
        with pytest.raises(ValueError):
            self.bank.withdraw_add(account_id, 500, currency='bla', tp='+')

        # 6
        with pytest.raises(ValueError):
            self.bank.withdraw_add(account_id, 500, currency='rub', tp='bla')

    def test_convert(self):
        """
        Test converting operation

        1) Check rub -> usd
        2) Check usd -> rub
        3) Check rub -> eur
        4) Check inncorrect input data
        """

        assert account.convert('rub', 'usd') == 0.01357
        assert account.convert('usd', 'rub') == 73.68
        assert account.convert('rub', 'eur') == 0.0111
        with pytest.raises(ValueError):
            account.convert('', '')
